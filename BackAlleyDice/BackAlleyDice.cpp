#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;


// function to print menu
void printMenu(int currMoney) {
	cout << "You have: " << currMoney << "credits" << endl;
	cout << "====================================================================" << endl;
	cout << "Bet Types:" << endl;
	cout << "1. Big            2. Small         3. Odd            4. Even" << endl;
	cout << "5. All 1s         6. All 2s        7. All 3s         8. All 4s" << endl;
	cout << "9. All 5s         10. All 6s       11. Double 1s     12. Double 2s" << endl;
	cout << "13. Double 3s     14. Double 4s    15. Double 5s     16. Double 6s" << endl;
	cout << "17. Any triples   18. 4 or 17      19. 5 or 16       20. 6 or 15" << endl;
	cout << "21. 7 or 14       22. 8 or 13      23. 9 or 12       24. 10 or 11" << endl;
	cout << "=====================================================================" << endl;
}

//function to calculate money won or lost, returns bet multiplier
bool calculate(string choice, int dice1, int dice2, int dice3) {
	//total calculated 
	int total = dice1 + dice2 + dice3;
	//if triple is checked
	bool isTriple = false;
	if (dice1 == dice2 && dice1 == dice3) {
		isTriple = true;
	}

	int userChoice = stoi(choice);

	//switch case for each bet choice
	switch (userChoice) {
		case 1: // BIG
			if (total >= 11 && total <= 17 && !isTriple) {
				return true;
			}
			break;
		case 2: // SMALL
			if (total >= 4 && total <= 10 && !isTriple) {
				return true;
			}
			break;
		case 3: // ODD
			if (!(total % 2 == 0) && !isTriple) {
				return true;
			}
			break;
		case 4: // EVEN
			if ((total % 2 == 0) && !isTriple) {
				return true;
			}
			break;
		case 5: // TRIPLE 1s
			if (dice1 == 1 && isTriple) {
				return true;
			}
			break;
		case 6: // TRIPLE 2s
			if (dice1 == 2 && isTriple) {
				return true;
			}
			break;
		case 7: // TRIPLE 3s
			if (dice1 == 3 && isTriple) {
				return true;
			}
			break;
		case 8: // TRIPLE 4s
			if (dice1 == 4 && isTriple) {
				return true;
			}
			break;
		case 9: // TRIPLE 5s
			if (dice1 == 5 && isTriple) {
				return true;
			}
			break;
		case 10: // TRIPLE 6s
			if (dice1 == 6 && isTriple) {
				return true;
			}
			break;
		case 11: // DOUBLE 1s
			if (((dice1 == dice2 && dice1 == 1) || (dice2 == dice3 && dice2 == 1)
				|| (dice1 == dice3 && dice1 == 1)) && !isTriple) {
				return true;
			}
			break;
		case 12: // DOUBLE 2s
			if (((dice1 == dice2 && dice1 == 2) || (dice2 == dice3 && dice2 == 2)
				|| (dice1 == dice3 && dice1 == 2)) && !isTriple) {
				return true;
			}
			break;
		case 13: // DOUBLE 3s
			if (((dice1 == dice2 && dice1 == 3) || (dice2 == dice3 && dice2 == 3)
				|| (dice1 == dice3 && dice1 == 3)) && !isTriple) {
				return true;
			}
			break;
		case 14: // DOUBLE 4s
			if (((dice1 == dice2 && dice1 == 4) || (dice2 == dice3 && dice2 == 4)
				|| (dice1 == dice3 && dice1 == 4)) && !isTriple) {
				return true;
			}
			break;
		case 15: // DOUBLE 5s
			if (((dice1 == dice2 && dice1 == 5) || (dice2 == dice3 && dice2 == 5)
				|| (dice1 == dice3 && dice1 == 5)) && !isTriple) {
				return true;
			}
			break;
		case 16: // DOUBLE 6s
			if (((dice1 == dice2 && dice1 == 6) || (dice2 == dice3 && dice2 == 6)
				|| (dice1 == dice3 && dice1 == 6)) && !isTriple) {
				return true;
			}
			break;
		case 17: // ANY TRIPLE
			if (isTriple) {
				return true;
			}
			break;
		case 18: // 4 OR 17
			if (total == 4 || total == 17) {
				return true;
			}
			break;
		case 19: // 5 OR 16
			if (total == 5 || total == 16) {
				return true;
			}
			break;
		case 20: // 6 OR 15
			if (total == 6 || total == 15) {
				return true;
			}
			break;
		case 21: // 7 OR 14
			if (total == 7 || total == 14) {
				return true;
			}
			break;
		case 22: // 8 OR 13
			if (total == 8 || total == 13) {
				return true;
			}
			break;
		case 23: // 9 OR 12
			if (total == 9 || total == 12) {
				return true;
			}
			break;
		case 24: // 10 OR 11
			if (total == 10 || total == 11) {
				return true;
			}
			break;
	}


	// false if you lost dice roll
	return false;
}

// function to generate 3 random dice numbers
bool rollDice(string choice) {
	//random between 1 and 7 (exclusive)
	int dice1 = (rand() % 6) + 1;
	int dice2 = (rand() % 6) + 1;
	int dice3 = (rand() % 6) + 1;

	// print rolled dice result
	cout << "==================" << endl;
	cout << "Rolling smuggler dice..." << endl;
	cout << endl;
	cout << "Dice 1: " << dice1 << endl;
	cout << "Dice 2: " << dice2 << endl;
	cout << "Dice 3: " << dice3 << endl;
	cout << endl;
	cout << "Sum: " << dice1 + dice2 + dice3 << endl;
	cout << "==================" << endl;

	//call calculate to determine if win or lose
	return calculate(choice, dice1, dice2, dice3);
}

// function to check if game has finished by checking player money
bool isGameFinished(int currMoney) {
	bool game = true;

	if (currMoney <= 0 || currMoney >= 100000) {
		game = false;
	}
	else {
		game = true;
	}

	return game;
}

// prints the lovely C3PO
static void printC3PO()
{
	cout << "        // ~\\         " << endl;
	cout << "       | o o)         " << endl;
	cout << "        _\\=/ _       " << endl;
	cout << "   #   /  _  \\   #   " << endl;
	cout << "     \\//|/.\\|\\//    " << endl;
	cout << "     \\/  \\_ /  \\/    " << endl;
	cout << "        |\\ /|        " << endl;
	cout << "        \\_ _/        " << endl;
	cout << "        | | |        " << endl;
	cout << "        | | |        " << endl;
	cout << "        [] |[]       " << endl;
	cout << "        | | |        " << endl;
	cout << "   ___ / _]_[_\\__    " << endl;
}

// prints the awesome Millenium Falcon
static void printWin()
{
	cout << "               c == o   " << endl;
	cout << "            _ / ____\\_  " << endl;
	cout << "      _.,--''' ||^ || ''`z._  " << endl;
	cout << "   / _ /^ ___\\||  || _ / o\\ '`-._   " << endl;
	cout << "  _ /  ]. L_ | || .||  \\_ / _._`--._    " << endl;
	cout << "  / _~7  _. '' ||. || /] \\ ]. (_)  . ''`--.   " << endl;
	cout << "  | __7~.(_)_[]| +--+|/ ____T_____________L   |  " << endl;
	cout << "  | __ | _ ^ (_) /^ __\\____ _   _ | " << endl;
	cout << "  | __ | (_){  _) J ]K{ __ L___ _ _] " << endl;
	cout << "  | __ | ._(_) \v / __________ | ________ " << endl;
	cout << "  l__l_(_). [] | +-+-<\\^ L._ - ---L | " << endl;
	cout << "   \\__\\    __. ||^ l  \\Y] / _]  (_)._,--'  |" << endl;
	cout << "    \\~_]  L_ | || .\\ .\\/ ~._,--''' " << endl;
	cout << "     \\_\\ . __ /||  |\\  \\`-+-< ''' " << endl;
	cout << "          ''`---._|J__L|X o~~|[\\      " << endl;
	cout << "              \\____ / \\___ |[//      " << endl;
	cout << "                `--'   `--+-'");
}

// function to print beginning dialogue
void beginDialogue() {
	// red dialogue is Jabba
	//Console.ForegroundColor = ConsoleColor.Red;   FIXME
	cout << "'Jabba the Hutt: Chuba! Bargon won che copa.'" << endl;
	enterToCont();

	// yellow dialogue is C3PO
	//Console.ForegroundColor = ConsoleColor.Yellow;   FIXME
	printC3PO();
	cout << "'C3PO: Oh my..His High Exaltedness the great Jabba the Hutt is angered that " << endl;
	cout << "you have failed to deliver the promised cargo'" << endl;
	enterToCont();

	cout << "'C3PO: However, in his generosity, the gracious Jabba will allow you to gamble for your life.'" << endl;
	cout << "'Win enough money to buy your freedom, or...oh..or be fed to the hi-hideous SARLAC!'" << endl;

	cout << endl;
	enterToCont();

}

//wait for user to press enter, stops screen from being cluttered
static void enterToCont()
{
	cout << "Press enter to continue....." << endl;
	cin.ignore();
}

// 3PO says a random phrase if you lose roll
static void randomC3PO()
{
	int chance = rand() % 6;


	string choices[5] = { "We were made to suffer.", "I can't bear to watch..", "Oh R2 we must do something!",
	"Oh if only Master Luke were here.", "I wonder if Jaba will hang you on the wall like poor Han Solo." };

	cout << choices[chance] << endl;
}

// main game
int main()
{
	// array of bet choices
	string choices[24] = { "Big", "Small", "Odd", "Even", "All 1s", "All 2s", "All 3s",
	"All 4s", "All 5s", "All 6s", "Double 1s", "Double 2s", "Double 3s", "Double 4s", "Double 5s",
	"Double 6s", "Any triples", "4 or 17", "5 or 16", "6 or 15", "7 or 14", "8 or 13", "9 or 12",
	"10 or 11" }; // string array [0-23]

	// array of bet multipliers, coincide with choices array
	int multipliers[24] = { 1, 1, 1, 1, 180, 180, 180, 180, 180, 180, 10, 10, 10, 10, 10, 10,
		30, 60, 30, 18, 12, 8, 7, 6};

	//start with 200
	int currMoney = 200;

	// vars for user money input
	string moneyChange = "n";
	int moneyCh = 0;
	bool res = false;

	//vars for user bet input
	string firstChoice = "n";
	int firstCh = 0;

	string playAgain = "yes";

	// keep playing until == "no"
	while (playAgain == "yes") {
		currMoney = 200;
		beginDialogue();
		//Console.ForegroundColor = ConsoleColor.White;   FIXME
		//main game loop
		while (isGameFinished(currMoney))
		{
			//menu printed with updated money
			//Console.ForegroundColor = ConsoleColor.White;     FIXME
			printMenu(currMoney);
			//Console.ForegroundColor = ConsoleColor.Yellow;    FIXME
			//loops until correct input is given
			while (!res)
			{
				cout << "C3PO: Oh dear...What bet would you like to make?" << endl;
				//choose bet type
				cin >> firstChoice;
				res = int.TryParse(firstChoice, out firstCh);
				if (res == false)
				{
					Console.WriteLine("C3PO: I must insist you take this seriously.");
				}
			}
			res = false;
			//checks bet, cannot bet more money than you have or <= 0
			while (!res)
			{
				Console.WriteLine("C3PO: You selected '{0}', how much would you like to bet?", choices[firstCh - 1]);
				//choice bet amount
				moneyChange = Console.ReadLine();
				res = int.TryParse(moneyChange, out moneyCh);
				//checks money input 
				if (moneyCh > currMoney)
				{
					res = false;
					Console.WriteLine("C3PO: But sir! You can't bet more money than you have!");
				}
				else if (moneyCh <= 0)
				{
					res = false;
					Console.WriteLine("C3PO: You must bet at least 1 credit.");
				}
			}
			//checks diceRoll result, true if win, false if lose
			if (!rollDice(firstChoice)) {
				Console.WriteLine("C3PO: Oh my! You lost {0} credits!", moneyCh);
				randomC3PO();
				//negate money originally bet
				moneyCh = (-moneyCh);
				enterToCont();
			}
			else {
				Console.WriteLine("C3PO: Thank the maker! You won {0} credits!", moneyCh * multipliers[firstCh - 1]);
				//multiply bet by choice multiplier
				moneyCh = (moneyCh * multipliers[firstCh - 1]);
				enterToCont();

			}
			// will add or subtract money based on calculation
			currMoney += moneyCh;

		}
		//game end, check if win/lose
		if (currMoney <= 0) {
			Console.WriteLine("YOU LOSE");
			Console.WriteLine("YOUR BODY WAS FED TO THE SARLAC");
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("'Jabba the Hutt: Ho Ho Ho Ho Ah hah hah hah'");
			Console.WriteLine("'Jabba the Hutt: Yoka to Bantha poodoo'");
			Console.ReadLine();
		}
		else {
			Console.WriteLine("C3PO: 'THANK THE MAKER!'");
			printWin();
		}
		//play again?
		Console.WriteLine("Play again?(yes/no)");
		playAgain = Console.ReadLine();

		return 0;
}
